The package for preventing dependency hijacking.

Learn more: 

- [Preventing Dependency Confusion in PHP with Composer](https://blog.packagist.com/preventing-dependency-hijacking/)